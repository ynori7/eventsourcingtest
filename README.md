Setup Instructions
------------------

1. Create the database using the schema defined in config/database.sql (may require adjustments since it was created for MySQL)
2. Copy config/parameters.yml.dist to config/parameters.yml and adjust the parameters
3. Install vendors (composer install)





Running The Tests
-----------------

Run the tests like this:

```
php vendor/bin/phpunit -c phpunit.xml
//depending on which version of phpunit you have it might be this instead:
./vendor/bin/phpunit -c phpunit.xml
```





Project Structure
-----------------

The project has the following directories:

* config - contains the config files and db schema
* src - the source code for the project
    * CommandHandlers - The main entry points for the project
    * Model - The model layer for the project
    * Services - Contains any services which are used across the project
* tests - the unit tests (also in this case the executable portion of the code)
