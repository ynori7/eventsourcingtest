<?php

namespace CommandHandlers;

use Model\TransactionEvent;
use Model\EventStoreRepository;
use Services\DatabaseService;

class TransactionHandler
{

    private $eventStream = array();

    /**
     * @param string $streamId
     * @param \DateTime $eventTimestamp
     * @param float $balance
     */
    public function appendDepositEvent($streamId, $eventTimestamp, $balance)
    {
        array_push($this->eventStream, new TransactionEvent($streamId,
            TransactionEvent::DEPOSIT_EVENT, $eventTimestamp, $balance));
    }

    /**
     * @param string $streamId
     * @param \DateTime $eventTimestamp
     * @param float $balance
     */
    public function appendDebitEvent($streamId, $eventTimestamp, $balance)
    {
        array_push($this->eventStream, new TransactionEvent($streamId,
            TransactionEvent::DEBIT_EVENT, $eventTimestamp, $balance));
    }

    /**
     * @param string $streamId
     * @param \DateTime $eventTimestamp
     * @param float $balance
     */
    public function appendCreditEvent($streamId, $eventTimestamp, $balance)
    {
        array_push($this->eventStream, new TransactionEvent($streamId,
            TransactionEvent::CREDIT_EVENT, $eventTimestamp, $balance));
    }

    /**
     * @param string $streamId
     * @param \DateTime $eventTimestamp
     * @param float $balance
     */
    public function appendWithDrawlEvent($streamId, $eventTimestamp, $balance)
    {
        array_push($this->eventStream, new TransactionEvent($streamId,
            TransactionEvent::WITHDRAWAL_EVENT, $eventTimestamp, $balance));
    }

    /**
     * Flush the event stream
     */
    public function processEvents()
    {
        $repo = new EventStoreRepository();
        $repo->setDatabaseConnection(DatabaseService::getInstance()->getConnection());
        
        $repo->appendAll($this->eventStream);

        $this->eventStream = array();
    }

    /**
     * Replay the events till the specified timestamp to get the balance as of that date.
     * Can eventually add an additional parameter for timestamp to get the balance as of a specified date
     *
     * @param string $streamId
     * @return float
     */
    public function getBalance($streamId)
    {
        $balance = 0;

        $repo = new EventStoreRepository();
        $repo->setDatabaseConnection(DatabaseService::getInstance()->getConnection());

        $events = $repo->findAll($streamId);

        foreach ($events as $event) {
            /** @var $event \Model\TransactionEvent */
            $data = $event->getEventData();

            $balance += $data['balance'];
        }

        return $balance;
    }
}