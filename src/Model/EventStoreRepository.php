<?php

namespace Model;

class EventStoreRepository implements EventStore
{
    /**
     * @var $connection \Doctrine\DBAL\Connection
     */
    private $connection;

    /**
     * @param array $changes
     */
    public function appendAll($changes)
    {
        $doctrine = $this->getDatabaseConnection();

        foreach ($changes as $change) {
            /** @var $change \Model\DomainEvent */

            $query = $doctrine->createQueryBuilder()
                ->insert('events')
                ->values(array(
                    'stream_id' => '?',
                    'timestamp' => '?',
                    'data' => '?',
                ))
                ->setParameter(0, $change->getStreamId())
                ->setParameter(1, $change->getRecordedTimestamp()->format('Y-m-d H:i:s'))
                ->setParameter(2, json_encode($change->getEventData()));

            $query->execute();
        }
    }

    /**
     * @param $streamId
     * @return array of DomainEvents
     */
    public function findAll($streamId)
    {
        $events = array();

        $doctrine = $this->getDatabaseConnection();

        $query = $doctrine->createQueryBuilder()
            ->select('*')
            ->from('events')
            ->where("stream_id = :id")
            ->setParameter('id', $streamId);

        $data = $query->execute()->fetchAll();

        foreach ($data as $event) {
            $domainEvent = new DomainEvent();
            $domainEvent->setStreamId($event['stream_id']);
            $domainEvent->setRecordedTimestamp(new \DateTime($event['timestamp']));
            $domainEvent->setEventData(json_decode($event['data'], true));

            array_push($events, $domainEvent);
        }

        return $events;
    }

    /**
     * @return \Doctrine\DBAL\Connection
     */
    public function getDatabaseConnection()
    {
        return $this->connection;
    }

    /**
     * @param \Doctrine\DBAL\Connection $connection
     */
    public function setDatabaseConnection($connection)
    {
        $this->connection = $connection;
    }
}