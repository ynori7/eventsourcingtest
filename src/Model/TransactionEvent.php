<?php

namespace Model;

class TransactionEvent extends DomainEvent
{
    const DEPOSIT_EVENT = 'deposit';
    const WITHDRAWAL_EVENT = 'withdrawal';
    const CREDIT_EVENT = 'credit';
    const DEBIT_EVENT = 'debit';

    /**
     * @param string $streamId
     * @param string $eventType
     * @param \DateTime $eventTimestamp
     * @param float $balance
     */
    public function __construct($streamId, $eventType, $eventTimestamp, $balance)
    {
        $this->setEventData(array(
            'eventType' => $eventType,
            'eventTimestamp' => $eventTimestamp,
            'balance' => $balance,
        ));

        $this->setStreamId($streamId);

        parent::__construct();
    }
}