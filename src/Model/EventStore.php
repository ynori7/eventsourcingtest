<?php

namespace Model;

interface EventStore
{
    /**
     * @param $changes (some collection of DomainEvents)
     */
    public function appendAll($changes);

    /**
     * @param $streamId
     * @return array of DomainEvents
     */
    public function findAll($streamId);

}