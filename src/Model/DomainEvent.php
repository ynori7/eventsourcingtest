<?php

namespace Model;

class DomainEvent
{
    private $streamId;
    private $recordedTimestamp; //internal timestamp of the time the event was recorded
    private $data;

    public function __construct()
    {
        $this->recordedTimestamp = new \DateTime();
    }

    /**
     * @param string $streamId
     */
    public function setStreamId($streamId)
    {
        $this->streamId = $streamId;
    }

    /**
     * @return string
     */
    public function getStreamId()
    {
        return $this->streamId;
    }

    /**
     * @return array
     */
    public function getEventData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setEventData($data)
    {
        $this->data = $data;
    }

    /**
     * @param \DateTime $recordedTimestamp
     */
    public function setRecordedTimestamp($recordedTimestamp)
    {
        $this->recordedTimestamp = $recordedTimestamp;
    }

    /**
     * @return \DateTime
     */
    public function getRecordedTimestamp()
    {
        return $this->recordedTimestamp;
    }

}