<?php

namespace Services;

use Symfony\Component\Yaml\Yaml;

class DatabaseService
{
    const CONFIG = 'config/parameters.yml';

    private static $instance;

    /** @var \Doctrine\DBAL\Connection */
    private $connection;

    private function __construct()
    {
        $parameters = Yaml::parse(file_get_contents(__DIR__ . '/../../' . self::CONFIG));
        $config = new \Doctrine\DBAL\Configuration();
        $this->connection = \Doctrine\DBAL\DriverManager::getConnection($parameters, $config);
    }

    /**
     * @return \Doctrine\DBAL\Connection
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @return DatabaseService
     */
    public static function getInstance()
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}