<?php
use CommandHandlers\TransactionHandler;
use Services\DatabaseService;

/**
 * @covers CommandHandlers\TransactionHandler
 */
class TransactionHandlerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var $handler \CommandHandlers\TransactionHandler
     */
    protected $handler;

    public function setUp()
    {
        $this->handler = new CommandHandlers\TransactionHandler();

        $db = DatabaseService::getInstance();
        $db->getConnection()->beginTransaction();
    }

    public function tearDown()
    {
        $db = DatabaseService::getInstance();
        $db->getConnection()->rollBack();
    }

    public function testInitialDeposit()
    {
        $streamId = '12345667';
        $expectedBalance = 10.50;

        $this->handler->appendDepositEvent($streamId, new DateTime("2014-03-05 14:00:21"), $expectedBalance);
        $this->handler->processEvents();

        $actualBalance = $this->handler->getBalance($streamId);

        self::assertEquals($expectedBalance, $actualBalance);
    }

    public function testMultipleDeposits()
    {
        $streamId = '12345667';
        $expectedBalance = 30.50;

        $this->handler->appendDepositEvent($streamId, new DateTime("2014-03-05 14:00:21"), 10.50);
        $this->handler->appendDepositEvent($streamId, new DateTime("2014-03-05 14:04:21"), 20);
        $this->handler->processEvents();

        $actualBalance = $this->handler->getBalance($streamId);

        self::assertEquals($expectedBalance, $actualBalance);
    }

    public function testWithdrawl()
    {
        $streamId = '12345667';
        $expectedBalance = 10.50;

        $this->handler->appendDepositEvent($streamId, new DateTime("2014-03-05 14:00:21"), 30.50);
        $this->handler->appendWithdrawlEvent($streamId, new DateTime("2014-03-05 14:04:21"), -20);
        $this->handler->processEvents();

        $actualBalance = $this->handler->getBalance($streamId);

        self::assertEquals($expectedBalance, $actualBalance);
    }

}