CREATE DATABASE event_sourcing;

USE event_sourcing;

CREATE TABLE events (
  event_id INT(10) AUTO_INCREMENT PRIMARY KEY, -- I guess this should probably be SERIAL for Postgres
  stream_id VARCHAR(255),
  timestamp DATETIME,
  data TEXT
);